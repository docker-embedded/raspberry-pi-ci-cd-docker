FROM arm32v7/debian:latest

# Update and install necessary packages
RUN apt-get update && apt-get dist-upgrade --yes
RUN apt-get install -y xinetd tftpd tftp gcc-arm*

# Add Python packages need for HIL testing
RUN apt-get install -y --no-install-recommends \
		python3 \
		python3-setuptools \
		python3-pip \
		python3-dev

# Install Python packages
RUN pip3 install wheel

RUN pip3 install \
    serial \
	pyserial \
	pytest \
    xunitparser \
	xunitparser \
	rpi.gpio
